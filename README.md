# shelf_oauth_memcache

[![Build Status](https://drone.io/bitbucket.org/andersmholmgren/shelf_oauth_memcache/status.png)](https://drone.io/bitbucket.org/andersmholmgren/shelf_oauth_memcache/latest)
[![Pub Version](http://img.shields.io/pub/v/shelf_oauth_memcache.svg)](https://pub.dartlang.org/packages/shelf_oauth_memcache)

Memcache (appengine flavour) oauth token persistence

## Usage

TODO...

## Features and bugs

Please file feature requests and bugs at the [issue tracker][tracker].

[tracker]: https://bitbucket.org/andersmholmgren/shelf_oauth_memcache/issues

