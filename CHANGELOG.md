# Changelog

## 0.3.6

* upgrade shelf_oauth

## 0.3.4

* upgrade shelf_oauth

## 0.3.0

* upgrade shelf, test and shelf_oauth

## 0.2.1+1

* fixed bug where memcache entry can be deleted before it is returned in some
cases

## 0.2.1

* broadened self_oauth versions

## 0.2.0

* corrected name of oauthStorage function

## 0.1.0

* oauth2 storage

## 0.0.1

* Initial version, created by Stagehand.
