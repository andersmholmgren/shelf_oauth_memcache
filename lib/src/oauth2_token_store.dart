// Copyright (c) 2015, The shelf_oauth authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_oauth_memcache.tokenstore.oauth2;

import 'package:shelf_oauth/shelf_oauth.dart';
import 'oauth2_token_store_impl.dart';
import 'package:memcache/memcache.dart';
import 'core.dart';

/// Creates an [OAuth2TokenStore] using [Memcache] for storage.
/// Secrets will expire after [expiration] duration which defaults to 2 minutes
OAuth2TokenStore oauth2TokenStore(MemcacheProvider memcacheProvider,
        {Duration expiration: const Duration(minutes: 2)}) =>
    new MemcacheOAuth2TokenStore(memcacheProvider, expiration);
