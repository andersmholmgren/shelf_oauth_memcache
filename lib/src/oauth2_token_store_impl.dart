// Copyright (c) 2015, The shelf_oauth authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_oauth_memcache.tokenstore.oauth2.impl;

import 'dart:async';
import 'package:option/option.dart';
import 'package:shelf_oauth/shelf_oauth.dart';
import 'core.dart';
import 'dart:convert';

class MemcacheOAuth2TokenStore extends BaseMemcacheStore
    implements OAuth2TokenStore {
  MemcacheOAuth2TokenStore(
      MemcacheProvider memcacheProvider, Duration maxTokenExpiration)
      : super(memcacheProvider, maxTokenExpiration);

  @override
  Future storeToken(String sessionIdentifier, OAuth2TokenPair tokenPair) {
    final expiry = tokenPair.expires.map((DateTime tokExpiry) {
      final tokenExpiration = tokExpiry.difference(new DateTime.now());
    }).getOrElse(() => null);

    return doStore(sessionIdentifier, JSON.encode(tokenPair),
        otherExpiration: expiry);
  }

  @override
  Future<Option<OAuth2TokenPair>> fetchToken(String sessionIdentifier) =>
      doFetch(sessionIdentifier).then((tokenPairJsonStrOpt) =>
          tokenPairJsonStrOpt
              .map((json) => new OAuth2TokenPair.fromJson(JSON.decode(json))));
}
