// Copyright (c) 2014, The shelf_oauth authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_oauth_memcache.tokenstore.oauth1.impl;

import 'dart:async';
import 'package:option/option.dart';
import 'package:shelf_oauth/shelf_oauth.dart';
import 'core.dart';

class MemcacheOAuth1RequestTokenSecretStore extends BaseMemcacheStore
    implements OAuth1RequestTokenSecretStore {
  MemcacheOAuth1RequestTokenSecretStore(
      MemcacheProvider memcacheProvider, Duration expiration)
      : super(memcacheProvider, expiration);

  @override
  Future storeSecret(String authToken, String secret) =>
      doStore(authToken, secret);

  @override
  Future<Option<String>> consumeSecret(String authToken) =>
      doConsume(authToken);
}
