// Copyright (c) 2014, The shelf_oauth authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_oauth_memcache.tokenstore.oauth1;

import 'package:shelf_oauth/shelf_oauth.dart';
import 'oauth1_token_store_impl.dart';
import 'package:memcache/memcache.dart';
import 'core.dart';

/// Creates an [OAuth1RequestTokenSecretStore] using [Memcache] for storage.
/// Secrets will expire after [expiration] duration which defaults to 2 minutes
OAuth1RequestTokenSecretStore oauth1TokenStore(
        MemcacheProvider memcacheProvider,
        {Duration expiration: const Duration(minutes: 2)}) =>
    new MemcacheOAuth1RequestTokenSecretStore(memcacheProvider, expiration);
