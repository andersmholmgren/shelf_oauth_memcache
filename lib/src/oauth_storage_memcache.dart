// Copyright (c) 2015, The shelf_oauth authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_oauth_memcache.storage.memcache;

import 'package:shelf_oauth/shelf_oauth.dart';
import 'core.dart';
import 'oauth_storage_memcache_impl.dart';

/// Creates an [OAuthStorage] using [Memcache] for storage.
///
/// [shortTermStorageExpiration] is the maximum duration that short term items
/// (for example states during an oauth dance) will remain in storage.
/// It defaults to 2 minutes
///
/// [sessionStorageExpiration] is the maximum duration that session type items
/// (for oauth tokens) will remain in storage. It defaults to 2 minutes
OAuthStorage oauthStorage(MemcacheProvider memcacheProvider,
        {Duration shortTermStorageExpiration: const Duration(minutes: 2),
        Duration sessionStorageExpiration: const Duration(hours: 1)}) =>
    new MemcacheOAuthStorage(
        memcacheProvider, shortTermStorageExpiration, sessionStorageExpiration);
