// Copyright (c) 2014, The shelf_oauth authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_oauth_memcache.tokenstore.core;

import 'package:memcache/memcache.dart';
import 'preconditions.dart';
import 'dart:async';
import 'package:option/option.dart';

/// A provider of [Memcache] instances. Note this is mainly to support appengine
/// style access to Memcache that occurs via a request scoped context
typedef Memcache MemcacheProvider();

abstract class BaseMemcacheStore<T> {
  final MemcacheProvider memcacheProvider;
  final Duration expiration;

  BaseMemcacheStore(this.memcacheProvider, this.expiration) {
    ensure(memcacheProvider, isNotNull);
    ensure(expiration, isNotNull);
  }

  Memcache get memcache {
    final m = memcacheProvider();
    ensure(m, isNotNull);
    return m;
  }

  Future doStore(String key, T value, {Duration otherExpiration}) {
    final exp = otherExpiration != null && otherExpiration <= expiration
        ? otherExpiration
        : expiration;

    return memcache.set(key, value, expiration: exp);
  }

  Future<Option<String>> doConsume(String key) {
    final result = doFetch(key);

    // remove it async. Don't care the outcome
    result.then((o) => o is Some ? memcache.remove(key) : null);

    return result;
  }

  Future<Option<String>> doFetch(String key) {
    return memcache.get(key).then((secret) => new Option(secret));
  }
}
