// Copyright (c) 2015, The shelf_oauth authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_oauth_memcache.oauth2.csrf.state.store;

import 'package:shelf_oauth/shelf_oauth.dart';
import 'oauth2_csrf_state_store_impl.dart';
import 'package:memcache/memcache.dart';
import 'core.dart';

/// Creates an [OAuth2CSRFStateStore] using [Memcache] for storage.
/// Secrets will expire after [expiration] duration which defaults to 2 minutes
OAuth2CSRFStateStore oauth2CSRFStateStore(MemcacheProvider memcacheProvider,
        {Duration expiration: const Duration(minutes: 2)}) =>
    new MemcacheOAuth2CSRFStateStore(memcacheProvider, expiration);
