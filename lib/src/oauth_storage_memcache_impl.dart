// Copyright (c) 2015, The shelf_oauth authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_oauth_memcache.storage.memcache;

import 'package:shelf_oauth/shelf_oauth.dart';
import 'core.dart';
import 'preconditions.dart';
import 'oauth1_token_store.dart' as oauth1;
import 'oauth2_token_store.dart' as oauth2;
import 'oauth2_csrf_state_store.dart' as oauth2_csrf;

class MemcacheOAuthStorage implements OAuthStorage {
  final MemcacheProvider memcacheProvider;
  final Duration shortTermStorageExpiration;
  final Duration sessionStorageExpiration;

  MemcacheOAuthStorage(this.memcacheProvider, this.shortTermStorageExpiration,
      this.sessionStorageExpiration) {
    ensure(memcacheProvider, isNotNull);
    ensure(shortTermStorageExpiration, isNotNull);
    ensure(sessionStorageExpiration, isNotNull);
  }

  OAuth1RequestTokenSecretStore _oauth1TokenSecretStore;

  @override
  OAuth1RequestTokenSecretStore get oauth1TokenSecretStore {
    if (_oauth1TokenSecretStore == null) {
      _oauth1TokenSecretStore = oauth1.oauth1TokenStore(memcacheProvider,
          expiration: shortTermStorageExpiration);
    }
    return _oauth1TokenSecretStore;
  }

  OAuth2TokenStore _oauth2TokenStore;

  @override
  OAuth2TokenStore get oauth2TokenStore {
    if (_oauth2TokenStore == null) {
      _oauth2TokenStore = oauth2.oauth2TokenStore(memcacheProvider,
          expiration: sessionStorageExpiration);
    }
    return _oauth2TokenStore;
  }

  OAuth2CSRFStateStore _oauth2CSRFStateStore;

  @override
  OAuth2CSRFStateStore get oauth2CSRFStateStore {
    if (_oauth2CSRFStateStore == null) {
      _oauth2CSRFStateStore = oauth2_csrf.oauth2CSRFStateStore(memcacheProvider,
          expiration: shortTermStorageExpiration);
    }
    return _oauth2CSRFStateStore;
  }
}
