// Copyright (c) 2015, The shelf_oauth authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_oauth_memcache.oauth2.csrf.state.store.impl;

import 'dart:async';
import 'package:option/option.dart';
import 'package:shelf_oauth/shelf_oauth.dart';
import 'core.dart';
import 'dart:convert';

class MemcacheOAuth2CSRFStateStore extends BaseMemcacheStore
    implements OAuth2CSRFStateStore {
  MemcacheOAuth2CSRFStateStore(
      MemcacheProvider memcacheProvider, Duration expiration)
      : super(memcacheProvider, expiration);

  @override
  Future storeState(OAuth2State state) =>
      doStore(state.state, JSON.encode(state));

  @override
  Future<Option<OAuth2State>> consumeState(String state) =>
      doConsume(state).then((strOpt) =>
          strOpt.map((json) => new OAuth2State.fromJson(JSON.decode(json))));
}
